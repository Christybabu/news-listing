import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsServiceService {
  constructor(private http: HttpClient) {}

  // calling API
  public getAllNews(newsType): Observable<any> {
    // API base url
    const API_URL = 'https://api.nytimes.com/svc/search/v2/';

    // API key value
    const API_KEY = '&api-key=ko6vxjDCZuMzmu8zPOyM0wKv5QnuUsPM';
    return this.http.get(API_URL + 'articlesearch.json?' + newsType + API_KEY).pipe(map(res => res));
  }
}
