import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturedNewsComponent } from './featured-news/featured-news.component';
import { LatestNewsComponent } from './latest-news/latest-news.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule} from '@angular/material/progress-spinner';


@NgModule({
  declarations: [FeaturedNewsComponent, LatestNewsComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    MatPaginatorModule,
    MatProgressSpinnerModule
  ],
  exports: [
    FeaturedNewsComponent, LatestNewsComponent
  ],
  providers: [
    HttpClient
  ]
})
export class NewsModule { }
