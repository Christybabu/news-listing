import { Component, OnInit } from '@angular/core';
import {NewsServiceService} from '../news-service.service';
@Component({
  selector: 'mf-featured-news',
  templateUrl: './featured-news.component.html'
})
export class FeaturedNewsComponent implements OnInit {
  featuredNewsData: any;
  isLoading = true;
  constructor(private newsServiceService: NewsServiceService) { }
  ngOnInit() {
    this.newsServiceService.getAllNews('q=a&fq=source:(The New York Times)').subscribe(featuredNews => {
      this.featuredNewsData = featuredNews.response.docs;
      this.isLoading = false;
    });
  }
}
