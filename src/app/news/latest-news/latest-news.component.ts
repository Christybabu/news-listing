import { Component, OnInit, ViewChild } from '@angular/core';
import { NewsServiceService } from '../news-service.service';
import { MatPaginator } from '@angular/material/paginator';


@Component({
  selector: 'mf-latest-news',
  templateUrl: './latest-news.component.html'
})
export class LatestNewsComponent implements OnInit {
  latestNewsData: any;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  lowValue = 0;
  pageSize = 50;
  pageIndex = 0;
  latestNewsDataToShow = [];
  isLoading = true;

  constructor(private newsServiceService: NewsServiceService) { }

  ngOnInit() {
    this.latestNews();
  }

  latestNews() {
    this.newsServiceService.getAllNews('q=a&fq=source:(The New York Times)').subscribe(latestNews => {
      this.latestNewsData = latestNews.response.docs;
      this.latestNewsDataToShow = this.latestNewsData.slice(0, 9);
      this.isLoading = false;
    });
    this.paginator.firstPage();
  }
 
  onPageChange($event) {
    // tslint:disable-next-line: max-line-length
    this.latestNewsDataToShow =  this.latestNewsData.slice($event.pageIndex*$event.pageSize, $event.pageIndex*$event.pageSize + $event.pageSize);
  }
}