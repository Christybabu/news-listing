// common module
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// material module
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';


// app module
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewsModule } from './news/news.module';
import {MatPaginatorModule} from '@angular/material/paginator';

// app components
import { HeaderComponent } from './header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NewsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatIconModule,
    MatPaginatorModule
  ],
  providers: [],
  bootstrap: [AppComponent, HeaderComponent]
})
export class AppModule { }
